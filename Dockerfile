# This is the image for the stock-bot app

# Use the self-built image as a parent image.
FROM registry.gitlab.com/ubermut.io/stock-bot/slim-python-chrome-headless:latest

# Set the working directory.
WORKDIR /usr/src/app

# Copy the file from your host to your current location.
COPY requirements.txt .

# Run the command inside your image filesystem.
RUN pip install -r requirements.txt

# Copy the rest of your app's source code from your host to your image filesystem.
COPY bot.py .

# Reset parent entry point
ENTRYPOINT []

# Run the specified command within the container.
CMD [ "python", "./bot.py" ]