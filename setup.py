import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="ubermut_stock_bot",
    version="0.0.1",
    author="Chan Ue Ting",
    author_email="chanueting@gmail.com",
    description="Telegram Stock Bot",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ubermut.io/stock-bot",
    packages=setuptools.find_packages(),
    license=license,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
)
