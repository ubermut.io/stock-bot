import asyncio
import base64
import configparser
import html
import io
import logging
import os
import sys
import traceback

import pyppeteer
import requests
import telegram

from functools import wraps

from requests_html import AsyncHTMLSession
from telegram import ParseMode
from telegram.utils.helpers import mention_html
from telegram.ext import Updater, CommandHandler
from telegram.ext.dispatcher import run_async

# Enable logging

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

logger = logging.getLogger(__name__)

# Define bot send action decorator
# https://github.com/python-telegram-bot/python-telegram-bot/wiki/Code-snippets#send-action-while-handling-command-decorator


def send_action(action):
    """Sends `action` while processing func command."""

    def decorator(func):
        @wraps(func)
        def command_func(update, context, *args, **kwargs):
            context.bot.send_chat_action(
                chat_id=update.effective_message.chat_id, action=action
            )
            return func(update, context, *args, **kwargs)

        return command_func

    return decorator


send_typing_action = send_action(telegram.ChatAction.TYPING)
send_upload_video_action = send_action(telegram.ChatAction.UPLOAD_VIDEO)
send_upload_photo_action = send_action(telegram.ChatAction.UPLOAD_PHOTO)


@run_async
@send_typing_action
def start(update, context):
    update.message.reply_text(
        "Hello! I am @ubermut_stock_bot!"
        "\nAvailable Commands"
        "\n/quote - Retrieve (delayed) quote"
        "\n"
        "\n Retrieve stock charts:"
        "\n/stock_intraday_chart - intraday"
        "\n/stock_daily_chart - daily"
        "\n/stock_monthly_chart - monthly"
        "\n/heatmap - S&P 500 heap map"
        "\n"
        "\n Source code: https://gitlab.com/ubermut.io/stock-bot",
        disable_web_page_preview=True,
    )


@run_async
@send_typing_action
def quote(update, context):
    if not context.args:
        update.message.reply_text(
            "/quote <TICKER>, for example: /quote AAPL or 0005.HK"
        )
    else:
        ticker = context.args[0].upper()
        quote = requests.get(
            "https://finnhub.io/api/v1/quote?symbol={0}&token={1}".format(
                ticker, finnhub_token
            )
        )

        json = quote.json()
        logger.info("Quote json: {0}".format(json))
        if json:

            previous_close = float(json["pc"]) if "pc" in json else 0
            difference = 0
            price = float(json["c"]) if "c" in json else 0

            if previous_close > 0:
                difference = (price - previous_close) / previous_close

            change = "+" if (difference >= 0) else "-"
            message = "Price: {0:.2f} ({1}{2:.2f}%)".format(
                price, change, abs(difference) * 100
            )
            update.message.reply_text(
                message,
                reply_to_message_id=update.message.message_id,
            )
        else:
            update.message.reply_text(
                "No matching ticker found",
                reply_to_message_id=update.message.message_id,
            )


@run_async
def stock_intraday_chart(update, context):
    stock_chart(update, context, "intraday")


@run_async
def stock_daily_chart(update, context):
    stock_chart(update, context, "daily")


@run_async
def stock_monthly_chart(update, context):
    stock_chart(update, context, "monthly")


@send_upload_photo_action
def stock_chart(update, context, interval):
    if not context.args:
        update.message.reply_text(
            "/stock_{0}_chart <TICKER>, for example: /stock_{0}_chart AAPL or 0005.HK".format(
                interval
            )
        )
    else:
        ticker = context.args[0].upper()
        quote = requests.get(
            "https://finnhub.io/api/v1/quote?symbol={0}&token={1}".format(
                ticker, finnhub_token
            )
        )

        if quote.json:

            if "." not in ticker:
                ticker += ".US"
            if interval == "intraday":
                period = 20
            elif interval == "daily":
                period = 45
            elif interval == "monthly":
                period = 50

            resq = requests.get(
                "http://charts.aastocks.com/servlet/Charts?fontsize=12&15MinDelay=T&lang=0"
                "&titlestyle=1&vol=1&Indicator=9&indpara1=20&indpara2=2&indpara3=-&indpara4=-"
                "&indpara5=-&scheme=1&com=100&chartwidth=672&chartheight=480&stockid={0}&period={1}"
                "&type=1&logoStyle=1&".format(ticker, period)
            )
            img = io.BytesIO(resq.content)
            update.message.reply_photo(
                img,
                caption="{0} {1} Chart (Copyright © AASTOCKS.com)".format(
                    ticker, interval.title()
                ),
                reply_to_message_id=update.message.message_id,
            )
        else:
            update.message.reply_text(
                "No matching ticker found",
                reply_to_message_id=update.message.message_id,
            )


@run_async
def snp_heatmap(update, context):
    asyncio.run(finviz_heatmap(update, context, "sec"))


@send_upload_photo_action
async def finviz_heatmap(update, context, market):
    session = AsyncHTMLSession()
    browser = await pyppeteer.launch(
        {
            "headless": True,
            "handleSIGINT": False,
            "handleSIGTERM": False,
            "handleSIGHUP": False,
            "ignoreDefaultArgs": True,
            "ignoreHTTPSErrors": True,
            "executablePath": "/headless-shell/headless-shell",
            "args": [
                "--no-sandbox",
                "--hide-scrollbars",
                "--mute-audio",
                "--single-process",
                "--disable-dev-shm-usage",
                "--disable-gpu",
                "--no-zygote",
                '--proxy-server="direct://"',
                "--proxy-bypass-list=*",
            ],
        }
    )

    session._browser = browser
    r = await session.get(f"https://finviz.com/map.ashx?t={market}")

    script = """
        () => {
        return document.querySelector("canvas.chart").toDataURL();
        }
        """

    try:
        canvas = await r.html.arender(script=script)
    finally:
        await browser.close()

    img = io.BytesIO(base64.b64decode(canvas.split(",")[1]))
    update.message.reply_photo(
        img,
        caption="S&P 500 heap map (Copyright © finviz.com)",
        reply_to_message_id=update.message.message_id,
    )


# this is a general error handler function. If you need more information about specific type of update, add it to the
# payload in the respective if clause
def error(update, context):
    # add all the dev user_ids in this list. You can also add ids of channels or groups.
    devs = telegram_bot_devs.split(",")
    # we want to notify the user of this problem. This will always work, but not notify users if the update is an
    # callback or inline query, or a poll update. In case you want this, keep in mind that sending the message
    # could fail
    if update.effective_message:
        text = (
            "Hey. I'm sorry to inform you that an error happened while I tried to handle your update. "
            "My developer(s) will be notified."
        )
        update.effective_message.reply_text(text)
    # This traceback is created with accessing the traceback object from the sys.exc_info, which is returned as the
    # third value of the returned tuple. Then we use the traceback.format_tb to get the traceback as a string, which
    # for a weird reason separates the line breaks in a list, but keeps the linebreaks itself. So just joining an
    # empty string works fine.
    trace = "".join(traceback.format_tb(sys.exc_info()[2]))
    # lets try to get as much information from the telegram update as possible
    payload = ""
    # normally, we always have an user. If not, its either a channel or a poll update.
    if update.effective_user:
        payload += f" with the user {mention_html(update.effective_user.id, update.effective_user.first_name)}"
    # there are more situations when you don't get a chat
    if update.effective_chat:
        payload += f" within the chat <i>{update.effective_chat.title}</i>"
        if update.effective_chat.username:
            payload += f" (@{update.effective_chat.username})"
    # but only one where you have an empty payload by now: A poll (buuuh)
    if update.poll:
        payload += f" with the poll id {update.poll.id}."
    # lets put this in a "well" formatted text
    text = (
        f"Hey.\n The error <code>{html.escape(str(context.error))}</code> happened{payload}."
        f"The full traceback:\n\n<code>{trace}"
        f"</code>"
    )
    # and send it to the dev(s)
    for dev_id in devs:
        context.bot.send_message(dev_id, text, parse_mode=ParseMode.HTML)
    # we raise the error again, so the logger module catches it. If you don't use the logger module, use it.
    raise


def main():
    # Define global variables

    global finnhub_token
    global telegram_bot_devs

    # Read config in local bot.ini or OS environmental variables

    ini_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), "bot.ini")
    config = configparser.ConfigParser()
    config.read(ini_path)
    telegram_token = config.get(
        "DEFAULT", "TELEGRAM_TOKEN", fallback=str(os.environ.get("TELEGRAM_TOKEN"))
    )
    finnhub_token = config.get(
        "DEFAULT", "FINNHUB_TOKEN", fallback=str(os.environ.get("FINNHUB_TOKEN"))
    )
    telegram_bot_devs = config.get(
        "DEFAULT",
        "TELEGRAM_BOT_DEVS",
        fallback=str(os.environ.get("TELEGRAM_BOT_DEVS")),
    )

    # Create the EventHandler and pass it your bot's token.
    updater = Updater(telegram_token, use_context=True)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # Define bot command handles
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", start))
    dispatcher.add_handler(CommandHandler("quote", quote))
    dispatcher.add_handler(CommandHandler("stock_intraday_chart", stock_intraday_chart))
    dispatcher.add_handler(CommandHandler("stock_daily_chart", stock_daily_chart))
    dispatcher.add_handler(CommandHandler("stock_monthly_chart", stock_monthly_chart))
    dispatcher.add_handler(CommandHandler("heatmap", snp_heatmap))

    # Log all errors
    dispatcher.add_error_handler(error)

    # Start the Bot
    if os.path.exists(ini_path):
        # DEV
        logger.info("Execute in DEV mode")
        updater.start_polling()
        # Run the bot until the you presses Ctrl-C or the process receives SIGINT,
        # SIGTERM or SIGABRT. This should be used most of the time, since
        # start_polling() is non-blocking and will stop the bot gracefully.
        updater.idle()

    else:
        # PROD
        logger.info("Execute in PROD mode")
        appname = str(os.environ.get("HEROKU_APP_NAME"))
        port_number = int(os.environ.get("PORT", "5000"))
        updater.start_webhook(
            listen="0.0.0.0", port=port_number, url_path=telegram_token
        )
        updater.bot.setWebhook(
            "https://{0}.herokuapp.com/".format(appname) + telegram_token
        )
        updater.idle()


if __name__ == "__main__":
    main()
