# Ubermut's Stock Bot
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

https://ubermut-stock-bot.herokuapp.com/

### bot_sample.ini
A sample bot.ini would ease local development. Copy the [bot_sample.ini](bot_sample.ini) to bot.ini and update the configuration which defined as follows:
```
[DEFAULT]
# https://core.telegram.org/bots#6-botfather
TELEGRAM_TOKEN = <Telegram API token>
TELEGRAM_BOT_DEVS = <Telegram User / Group Id, separated by commas>
# https://finnhub.io/
FINNHUB_TOKEN = <Finnhub API key>
```

### Environment variable on Heroku
Environment variables needs to be set up in Heroku in deployment.
```
TELEGRAM_TOKEN = <Telegram API token>
TELEGRAM_BOT_DEVS = <Telegram User / Group Id, separated by commas>
FINNHUB_TOKEN = <Finnhub API key>
HEROKU_APP_NAME = <Heroku app name>
```
Refer to [Heroku documentation - Configuration and Config Vars](https://devcenter.heroku.com/articles/config-vars) for details.
Or you could pass them using Gitlab CI variables as below.

### Environment variable on Gitlab CI
To ease deployment, Heroku environment variables could passed from Gitlab CI.
```
TELEGRAM_REVIEW_TOKEN = <Telegram API token for review app>
TELEGRAM_STAGING_TOKEN = <Telegram API token for staging app>
TELEGRAM_PROD_TOKEN = <Telegram API token for production app>
TELEGRAM_BOT_DEVS = <Telegram User / Group Id, separated by commas>
FINNHUB_TOKEN = <Finnhub API key>
HEROKU_REVIEW_API_KEY = <Heroku API key for review app>
HEROKU_STAGING_API_KEY = <Heroku API key for staging app>
HEROKU_PROD_API_KEY = <Heroku API key for production app>
```
while the HEROKU_APP_NAME is defined as $CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA.

Refer to [Gitlab documentation - Custom environment variables](https://gitlab.com/help/ci/variables/README#custom-environment-variables) for details.
(Also [Gitlab documentation - Predefined environment variables](https://gitlab.com/help/ci/variables/predefined_variables.md))

and [Heroku documentation on setting API key](https://help.heroku.com/PBGP6IDE/how-should-i-generate-an-api-key-that-allows-me-to-use-the-heroku-platform-api).

### Acknowledgments
- [Telegram Bot that does Singaporean Stuff](https://github.com/kianhean/ShiokBot)

